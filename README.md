## Java project Continuous Integration with Concourse CI
As pipeline looks for a file `../version/version` which is the version file kept in `version` branch and fetched by `concourse-semver-resource`. While working locally, make sure `../version/version` exists.

```
mkdir -p ../version
# echo -n version_number > ../version/version
echo -n 2.3.0 > ../version/version
```

Or in `build.gradle` change `version = new File('../version/version').text` to something like `version = 1.0.0`.
#### Hello world Java Vertx app
```
./gradlew build
./gradlew test
./gradlew run
```

Publish to Artifactory
```
./gradlew artifactoryPublish
```

Build fatjar
```
./gradlew shadowJar
```


#### Pipeline

- unit test
- sonarscan (static code ananlysis)
- artifactory (publish built binaries)
- dockerhub (publish docker images)
- deploy helm chart

![pipeline](images/pipeline.png)

Start pipeline

```
fly -t main login -c http://127.0.0.1:8080 -u test -p test
fly -t main sp -c ci/pipeline.yaml  -p concourse-ci-java
fly -t main up -p concourse-ci-java
```

Destroy pipeline
```
fly -t main dp  -p concourse-ci-java
```
