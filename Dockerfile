FROM openjdk:8-alpine
WORKDIR /verticle
COPY artifact/*.jar .
CMD ["sh", "-c", "java -jar $(ls *.jar)"]
EXPOSE 8888
